let trainer = {
	name: "William",
	age: 17,
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	friends: {
		hoenn: ["May", "Max"],
		kanto: ["Brock", "Misty"],
	},
	talk: function(index){
		console.log(this.pokemon[index] + " I choose you");
	}
}
console.log(trainer)
console.log("Result of dot notation")
console.log(trainer.name)
console.log("Result of square bracket notation")
console.log(trainer["pokemon"])
console.log("Result of talk method")
trainer.talk([0])

function Pokemon(name, level, health){
	this.name = name
	this.level = level
	this.health = health
	this.attack = level
	this.tackle = function(target){
		console.log(this.name+ " tackle " + target.name)
		let hp = target.health - this.attack
		console.log(target.name + " health is down to "+hp)
		if (hp>0) {
			target.health = hp
		}
		else{
			target.health = hp
		}
		this.faint = function(){
			console.log(this.name+" fainted!")
		}
	}

}
let pokemon1 = new Pokemon("pikachu", 12, 24);
console.log(pokemon1)
let pokemon2 = new Pokemon("geodude", 8, 16)
console.log(pokemon2)
let pokemon3 = new Pokemon("mewtwo", 100, 200)
console.log(pokemon3)

pokemon1.tackle(pokemon2)
console.log(pokemon2)

pokemon3.tackle(pokemon1)
pokemon1.faint()
console.log(pokemon1)
